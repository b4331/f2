#!/usr/bin/env bash
 
VCHAR="|"
HCHAR="-"
ECHAR="\u2731"

X_MAX=$(( `tput cols` - 1 ))
Y_MAX=$(( `tput lines` - 1 ))
 
start_x=$(( X_MAX - 12 ))
start_y=$(( Y_MAX / 2 ))
start_x=50
start_y=$(( Y_MAX - 5 ))

initial_y=10
ilosc_zyc=5
czas_trafienia=0
niesmiertelny=5
points=0
trafiony=0
INITIAL_DELAY=0.1
DELTA_DELAY=0.01
MAX_ENEMIES=20
ACTIVE_ENEMIES=0
DONE_ENEMIES=0
ACT_LEVEL=1
MAX_LEVEL=7

function init_vars() {
 
        tput clear
        stty -icanon min 0 -echo
	# usunecie znaku zachety
	printf "\e[?25l"
        > .a
 
}
 
function clear_() {
 
        stty sane
	trap '' SIGUSR1
	printf "\e[?25h"
	exit 0
}

function koniec() {

	MSG=$1
	clear
	tput cup $(( Y_MAX / 2 )) $(( X_MAX / 2 ))
	printf "GAME OVER - "
	sleep 3
	printf "\e[?25h"
	exit 0
}
	

function draw_line() {
# x
# y
# v
local x=$1
local y=$2
add_char=$4
   if [[ "$3" = 'h' ]]
   then
        for i in `seq 0 $X_MAX`
        do
                tput cup $x $(( 0 + i))
                echo -en "$add_char$HCHAR\e[0m"
        done
   fi
 
   if [[ "$3" = 'v' ]]
   then
        for i in `seq 0 $Y_MAX`
        do
                tput cup $(( 0 + i )) $y
                echo -en "$add_char$VCHAR\e[0m"
        done
   fi
}
 
function draw_frame() {
 
        draw_line 0 0 "h" "\e[48;5;237m"
        draw_line 0 0 "v" "\e[48;5;237m"
        draw_line $Y_MAX 0 "h" "\e[48;5;237m"
        draw_line 0 "$X_MAX" "v" "\e[48;5;237m"
	draw_line 8 0 "h" "\e[48;5;237m"
 
}

function info() {
	BLINK=""
	[[ "$trafiony" -eq 1 ]] && BLINK="\e[5m\e[91m" && ( tput cup 2 3 && echo "                     " )
	( tput cup 2 3 && echo -en "${BLINK}Ilosc zyc\e[0m: " && for i in `seq 1 $ilosc_zyc`; do ( tput cup 2 $(( 15 + $i )) && echo -e "${BLINK}\u2665\e[0m" );done )
	( tput cup 3 3 && echo -en "Punkty: $points\tstart_x: $start_x\tstart_y: $start_y\ttrafiony: $trafiony" )
	( tput cup 4 3 && printf "1: %-12s DONE: %-4s DELAY: %-3s LEVEL: %-s ACT_ENEM: %-2s" "${ENEMY1[y]} ${ENEMY1[x]} ${ENEMY1[active]}" "$DONE_ENEMIES" $INITIAL_DELAY $ACT_LEVEL $ACTIVE_ENEMIES )
	( tput cup 5 3 && printf "2: %-12s" "${ENEMY2[y]} ${ENEMY2[x]} ${ENEMY2[active]}" )
	( tput cup 6 3 && printf "3: %-12s" "${ENEMY3[y]} ${ENEMY3[x]} ${ENEMY3[active]}" )
}

function check_level() {
	[[ "$DONE_ENEMIES" -le 0 ]] && return
	if (  [[ $(( DONE_ENEMIES % 50 )) -eq 0 ]] ) 
		#&& ( [[ "$ACT_LEVEL" -lt "$MAX_LEVEL" ]] ) )
	then
		INITIAL_DELAY=`printf "%.2f" \`echo "$INITIAL_DELAY - $DELTA_DELAY" | bc -l\``
		DONE_ENEMIES=$(( DONE_ENEMIES + 1 ))
		MAX_ENEMIES=$(( MAX_ENEMIES + 2 ))
		ACT_LEVEL=$(( ACT_LEVEL + 1 ))
	fi
}
function activate_enemies() {
	
	[[ "$ACTIVE_ENEMIES" -ge "$MAX_ENEMIES" ]] && return 0

	
	for i in `seq 1 $MAX_ENEMIES`
	do
		reference_name=ENEMY$i
		reference_active=ENEMY$i[active]
		reference_x=ENEMY$i[x]
		reference_y=ENEMY$i[y]

	
	if [[ "${!reference_active}" -eq 0 ]] && [[ "$((RANDOM % 8 + 2))" -eq 2 ]]
	then
		ACTIVE_ENEMIES=$((ACTIVE_ENEMIES + 1))
		RANDOM_X=`shuf -i 1-$((\`tput cols\` - 2)) -n 1`
		eval ENEMY$i='([x]=${RANDOM_X} [y]=$initial_y [active]=1)'
	fi
	done

}

function inc_delta() {

	for i in `seq 1 $MAX_ENEMIES`
	do
		reference_active=ENEMY$i[active]
		reference_y=ENEMY$i[y]

		[[ "${!reference_active}" -eq 1 ]] && eval ENEMY$i[y]=$(( ENEMY$i[y] + 1 ))
	done
}
function draw_car() {
	[[ "$trafiony" -eq 1 ]] && COL="$COL\e[47m"
        [[ "$1" = "black" ]] && COL=$BLACK
	( tput cup $start_y $start_x && echo -en "${COL}<[VXV]>\e[0m" )
        COL=$CAR_COLOR
}
function check_time_after_hit() {
	[[ $(( SECONDS - czas_trafienia)) -ge "$niesmiertelny" ]] && trafiony=0 && draw_car
}
#function draw_ca() {
#
#        [[ "$1" = "black" ]] && COL=$BLACK
#        tput cup $start_x $start_y 
#	echo -en "${COL}O-/|\-O\e[0m" 
#	COL=$CAR_COLOR
#}

function draw_enemy() {
	## Dodatkowy escape char dla koloru - enemy
	escchar="\e[91m"
	[[ "$3" -eq 0 ]] && return 0
	REM=0
	[[ "$4" = "remove" ]] && escchar="\e[0m\e[30;49m" && REM="1"
	[[ "$4" = "final_remove" ]] && escchar="\e[0m\e[30;49m" && REM="0"
	local_y=$1
	ly=$(( local_y - REM ))
	( tput cup $ly $2 && echo -en "${escchar}$ECHAR\e[0m" )
	#unset REM
}



function mass_draw() {
	for i in `seq 1 $MAX_ENEMIES`
	do
		reference_active=ENEMY$i[active]
      		reference_x=ENEMY$i[x]
      		reference_y=ENEMY$i[y]
        draw_enemy ${!reference_y} ${!reference_x} ${!reference_active} 
	done
}	
function mass_undraw() {
        for i in `seq 1 $MAX_ENEMIES`
        do
                reference_active=ENEMY$i[active]
                reference_x=ENEMY$i[x]
                reference_y=ENEMY$i[y]
	        draw_enemy ${!reference_y} ${!reference_x} ${!reference_active} remove
	done
}

function check_collision() {

	for i in `seq 1 $MAX_ENEMIES`
	do
		reference_active=ENEMY$i[active]
		reference_x=ENEMY$i[x]
		reference_y=ENEMY$i[y]
		# bez pustych przebiegów - jezeli nie aktywny, nie sprawdzamy
		[[ "${!reference_active}" -ne 1 ]] && continue	
		if [[ "${!reference_y}" -eq "$start_y" ]]
		then
			if [[ "${!reference_x}" =~ ^($start_x|$(( start_x + 1))|$(( start_x + 2))|$(( start_x + 3)))$ ]]
			then
				if [[ "$trafiony" -ne 1 ]]
				then
				ilosc_zyc=$(( ilosc_zyc - 1 ))
				[[ "$ilosc_zyc" -le 0 ]] && koniec
				trafiony=1
				czas_trafienia=$SECONDS
				draw_car
				fi
			fi
		fi

		if [[ "${!reference_y}" -ge $((Y_MAX -2 )) ]] 
		then
			# w tej instrukcji sa tylko aktywni, wiec po przekroczeniu ekranu, dodajemy punkty
			points=$(( points + 1 ))
			DONE_ENEMIES=$(( DONE_ENEMIES + 1 ))
			draw_enemy ${!reference_y} ${!reference_x} ${!reference_active} final_remove 
			eval ENEMY$i[active]=0 
			ACTIVE_ENEMIES=$((ACTIVE_ENEMIES - 1))
		fi
	done
}

function check_car_pos() {

	[[ "$start_x" -lt 1 ]] && start_x=1
	[[ "$start_x" -gt $(( X_MAX - 8 )) ]] && start_x=$((`tput cols` - 8 ))
	[[ "$start_y" -gt $(( Y_MAX - 2 )) ]] && start_y=$((`tput lines` - 2 ))
	[[ "$start_y" -lt "$initial_y" ]] && start_y=$initial_y
}

function move_enemies() {

	( sleep $INITIAL_DELAY; kill -s SIGUSR1 $$ ) &
	activate_enemies
	mass_undraw
	mass_draw
	check_collision
	check_time_after_hit
	info
	#draw_car black
	#draw_car
	#check_level
	inc_delta
}	

#WHITE="\e[48;5;247m"
CAR_COLOR="\e[38;5;147m"
BLACK="\e[30m"
COL=$CAR_COLOR
trap move_enemies SIGUSR1

for i in `seq 1 $MAX_ENEMIES`; do declare -A ENEMY$i; done
init_vars
draw_frame
move_enemies
info
while true;
do
        #debug
	info
        read -rs -n1 znak
 
        [[ "$znak" = 'q' ]] && clear_
 
        if [[ "$znak" = 'a' ]]
        then
		draw_car black
                ((start_x--))
		check_car_pos
		draw_car
        elif [[ "$znak" = 'd' ]]
        then
		draw_car black
                ((start_x++))
		check_car_pos
		draw_car
        elif [[ "$znak" = 'w' ]]
        then
		draw_car black
                ((start_y--))
		check_car_pos
		draw_car
        elif [[ "$znak" = 's' ]]
        then
		draw_car black
                ((start_y++))
		check_car_pos
		draw_car
        elif [[ "$znak" = 'p' ]]
        then
                DEBUG=YES
        fi
done
